#include<stdio.h>
char inputsymb[10];
int matrix[20][20];
int visited[20]={0};
int closure[20];
int count=0;
	
void update_closure(int state, int input)
{
	if(visited[state]==1)
	{
		closure[count]=-1;
		return;
	}
	closure[count++]=state;
	visited[state]=1;
	state = matrix[state][input];
	update_closure(state,input);
}
	
void main()
{
	int number_of_states,number_of_alphabets,i,j,state;
	printf("\nEnter no. of states: ");
	scanf("%d",&number_of_states);	
	printf("\nEnter no. of input alphabets (excluding epsilon): ");
	scanf("%d",&number_of_alphabets);
	for(i=0;i<number_of_alphabets;i++)
	{
		printf("\nEnter input alphabet %d: ",i+1);
		do
		{
			inputsymb[i] = getchar();
		}while(inputsymb[i]=='\n'); 
	}
	inputsymb[i]='E';
	printf("\nInput the transition table: \n\n");
	for(i=0;i<number_of_states;i++)
		for(j=0;j<number_of_alphabets+1;j++)
		{
			printf("State %d on input alphabet %c goes to: ",i,inputsymb[j]);
			scanf("%d",&matrix[i][j]);
			if(matrix[i][j] > number_of_states -1) 
				matrix[i][j] = -1;
		}
	do{
	printf("\nEpisilon closure of : ");
	scanf("%d",&state);
	if(state == -1)
		break;
	if(state > number_of_states - 1){
		printf("Invalid State");
	}
	else{
	update_closure(state,number_of_alphabets);
	printf("Epsilon Closure of %d is: ",state);
	for(i=0;i<20;i++)
		if(closure[i]!=-1)
			printf("%d ",closure[i]);
		else
			break;	
	printf("\n");
	int x[20] ={20};
	*visited = *x;
}
}while(state != -1);
}

