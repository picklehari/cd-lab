#include<stdio.h>
char inputsymb[10];
int matrix[20][20];
int visited[20]={0};
int closure[20];
int count=0;
	
void func(int state, int input)
{
	if(visited[state]==1)
	{
		closure[count]=-1;
		return;
	}
	closure[count++]=state;
	visited[state]=1;
	state = matrix[state][input];
	func(state,input);
}
	
void main()
{
	int n,a,i,j,state;
	printf("\nEnter no. of states: ");
	scanf("%d",&n);	
	printf("\nEnter no. of input alphabets (excluding epsilon): ");
	scanf("%d",&a);
	for(i=0;i<a;i++)
	{
		printf("\nEnter input alphabet %d: ",i+1);
		do
		{
			inputsymb[i] = getchar();
		}while(inputsymb[i]=='\n'); 
	}
	inputsymb[i]='E';
	printf("\nInput the transition table (enter -1 for invalid transition, beginning state is 0): \n\n");
	for(i=0;i<n;i++)
		for(j=0;j<a+1;j++)
		{
			printf("State %d on input alphabet %c goes to: ",i,inputsymb[j]);
			scanf("%d",&matrix[i][j]);
		}
	printf("\nWhich state's epsilon closure is to be found? ");
	scanf("%d",&state);
	func(state,a);
	printf("Epsilon Closure of %d is: ",state);
	for(i=0;i<20;i++)
		if(closure[i]!=-1)
			printf("%d ",closure[i]);
		else
			break;	
	printf("\n");
//	for(i=0;i<n;i++)
//	{
//		for(j=0;j<a+1;j++)
//			printf("%d",matrix[i][j]);		 
//		printf("\n");
//	}
}
