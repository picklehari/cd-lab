
%{
    #include <stdio.h>
    #include "y.tab.c" 
%}
  
%% 
[a-z A-Z]|[a-z A-Z][a-z A-Z 0-9]* {return B;} 
\n {return NL;} 
.  {return yytext[0];} 
%% 
  
int yywrap() {return 1;};