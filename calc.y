%{
#include<stdio.h>
int flag =0;
%}

%token NUMBER 
%left '+' '-'
%left '*' '/' '%'
%left '(' ')'

%%
AE: E 
{ 
  
  printf("Result = %d\n", $$); 
  return 0; 
  
 };
E: E'+'E {$$ = $1 + $3; printf("\n%d",$$);}
|E'-'E {$$ = $1 - $3; printf("\n%d",$$);}
|E'*'E {$$ = $1 * $3; printf("\n%d",$$);}
|E'/'E {$$ = $1 / $3; printf("\n%d",$$);}
|E'%'E {$$ = $1 % $3; printf("\n%d",$$);}
|'('E')' {$$ = $2; printf("\n%d",$$);}
|NUMBER {$$ = $1; printf("\n%d",$$); }

%%

void main(){
    printf("Enter Operation");
    yyparse();

}
